## [1.0.1](https://gitlab.com/czerasz/sample-app-helm-chart/compare/v1.0.0...v1.0.1) (2020-09-27)


### Bug Fixes

* add missing namespace ([c168ab7](https://gitlab.com/czerasz/sample-app-helm-chart/commit/c168ab70c31cb8edb284522807f580f8221ceae7))

# 1.0.0 (2020-09-27)


### Bug Fixes

* add missing package ([ebca887](https://gitlab.com/czerasz/sample-app-helm-chart/commit/ebca8874b1a8509b69fcc45fc8604b91dabcd91a))
* CI configuration syntax ([6eec7de](https://gitlab.com/czerasz/sample-app-helm-chart/commit/6eec7de01b3a5423910c848b41de66499d0c5e0d))
* ci job dependency ([5201463](https://gitlab.com/czerasz/sample-app-helm-chart/commit/5201463f350ae054f2cc78e057d60a71af18f277))
* CI remove obsolete dependency referal ([004653e](https://gitlab.com/czerasz/sample-app-helm-chart/commit/004653e6a0c0e24b5fd70680afe2fc9db5d10a8c))
* prometheus annotations ([98a92f8](https://gitlab.com/czerasz/sample-app-helm-chart/commit/98a92f814e14db1154bf123bbbb8900c4b948a48))


### Code Refactoring

* adjust chart to application needs ([cd0dfb5](https://gitlab.com/czerasz/sample-app-helm-chart/commit/cd0dfb519b1244e9a375ef14c4fc2b3bd0420f67))
* update Docker image ([ba1d7c8](https://gitlab.com/czerasz/sample-app-helm-chart/commit/ba1d7c82fcfc788f3a0a6ab14a7f39ba0b193acf))


### Continuous Integrations

* add CI configuration ([b33e6e9](https://gitlab.com/czerasz/sample-app-helm-chart/commit/b33e6e983a8f2168b8eb6a7b8f9f345e83c73939))


### Documentation

* add documentation ([e733196](https://gitlab.com/czerasz/sample-app-helm-chart/commit/e733196bdc6483f298a9b166f1dab23978cd8c27))


### feature

* generate Helm chart ([0f7989d](https://gitlab.com/czerasz/sample-app-helm-chart/commit/0f7989d3e6f1538256a846b2dec01923fc64c5e2))


### Features

* add istio support ([fba3454](https://gitlab.com/czerasz/sample-app-helm-chart/commit/fba34544e061cc72d0898fac1f5e298c48e9c78b))
