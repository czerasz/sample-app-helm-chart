#!/usr/bin/env bash

set -e

script_directory="$( cd "$( dirname "$0" )" && pwd )"
project_directory="${script_directory}/.."

version="${1:-unknown}"

if [ "${version}" = 'unknown' ]; then
  echo 'required version argument missing'
  exit 1
fi

tar -xf "${project_directory}/helm-packages/chart-9999.9999.9999.tgz"
sed -i 's/9999\.9999\.9999/'"${version}"'/' "${project_directory}/app/Chart.yaml"
tar -czf "${project_directory}/chart.tgz" "${project_directory}/app/"
rm -r "${project_directory}/app/"