#!/usr/bin/env bash

set -e

script_directory="$( cd "$( dirname "$0" )" && pwd )"
project_directory="${script_directory}/.."

version="${HELM_VERSION:-3.3.4}"
checksum="${HELM_CHECKSUM:-b664632683c36446deeb85c406871590d879491e3de18978b426769e43a1e82c}"

if ! command -v curl &> /dev/null; then
    echo "curl could not be found"
    exit 1
fi

mkdir -p "${project_directory}/bin"
curl -sSLO "https://get.helm.sh/helm-v${version}-linux-amd64.tar.gz"
sha256sum "helm-v${version}-linux-amd64.tar.gz" | grep "${checksum}"
tar -xzf "helm-v${version}-linux-amd64.tar.gz"
mv linux-amd64/helm ./bin/
rm -r "helm-v${version}-linux-amd64.tar.gz" linux-amd64